package ru.t1.dkandakov.tm.exception.field;

public final class NameEmptyException extends AbstractFiledException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
