package ru.t1.dkandakov.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(final String message) {
        super(message);
    }

    public AbstractException(final String message, Throwable cause) {
        super(message, cause);
    }

    public AbstractException(final Throwable cause) {
        super(cause);
    }

    public AbstractException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
